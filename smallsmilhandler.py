#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.root_list = ['width', 'height', 'background-color']
        self.region_list = ['id', 'top', 'bottom', 'left', 'right']
        self.img_list = ['src', 'region', 'begin', 'dur', 'end']
        self.audio_list = ['src', 'begin', 'dur']
        self.textstream_list = ['src', 'region', 'fill']
        self.taggs = {'root-layout': self.root_list,
                      'region': self.region_list,
                      'img': self.img_list,
                      'audio': self.audio_list,
                      'textstream': self.textstream_list
                      }
        self.listt = []

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        dic = {}
        if name in self.taggs:
            dic['etiqueta'] = name
            for attrib in self.taggs[name]:
                dic[attrib] = attrs.get(attrib, '')
            self.listt.append(dic)

    def get_tags(self):
        return self.listt


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    ssHandler = SmallSMILHandler()
    parser.setContentHandler(ssHandler)
    parser.parse(open('karaoke.smil'))
    listt = ssHandler.get_tags()
    print(listt)

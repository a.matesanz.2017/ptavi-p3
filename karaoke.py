#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import smallsmilhandler
import json
import urllib.request

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class KaraokeLocal():
    """
    My class
    """

    def __init__(self, file):
        """
        Constructor. Inicializamos las variables
        """
        ssh = smallsmilhandler
        parser = make_parser()
        ssHandler = ssh.SmallSMILHandler()
        parser.setContentHandler(ssHandler)
        try:
            parser.parse(open(file))
        except FileNotFoundError:
            sys.exit("File not found")
        self.listt = ssHandler.get_tags()

    def __str__(self):
        """
        ej4 a partir de la lista devuelve un string
        """
        chain = ""
        for name in self.listt:
            for element in name:
                if element == 'etiqueta':
                    if chain != "":
                        chain = chain + '\n'
                    chain = chain + str(name[element]) + '\t'
                else:
                    e = str(element)
                    n = str(name[element])
                    chain = chain + e + '="' + n + '"' + '\t'
        return chain

    def to_json(self, file, resulting_file=""):
        """
        ej4 guarda en formato json
        """
        if resulting_file == "":
            file = file.split(".")
            resulting_file = file[0] + '.json'
        with open(str(resulting_file), 'w') as f:
            json.dump(self.listt, f)

    def do_local(self):
        """
        ej5 descarga resursos remotos
        """
        for name in self.listt:
            for element in name:
                if element == 'src':
                    if str(name[element])[0:4] == 'http':
                        s = str(name[element])
                        n = name[element].split("/")[-1]
                        l, headers = urllib.request.urlretrieve(s, n)
                        name[element] = name[element].split("/")[-1]


if __name__ == '__main__':
    """
    Main Program
    """
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 karaoke.py file.smil")
    _, file = sys.argv
    try:
        file = str(file)
    except IndexError:
        sys.exit("Wrong parameters")

    Karaoke_object = KaraokeLocal(file)

    # se imprime el objeto
    print(Karaoke_object)

    """
    llamo al metodo para descargar recursos remotos pasando un solo parametro
    si se obvia el nombre del archivo resultante, sera karaoke.json por defecto
    """
    Karaoke_object.to_json(file)

    # llamo al metodo para descargar resursos remotos
    Karaoke_object.do_local()

    # llamo al metodo to_json que lo guarda con otro nombre
    Karaoke_object.to_json(file, 'local.json')

    # imprimo otra vez el objeto
    print(Karaoke_object)
